package com.snapp.buyticket.dal.entity.mongo;

import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "Terminal")
public class Terminal extends BaseEntity{

    private String name;
    private ObjectId cityId;
    private String cityName;
    private String phone;

    public enum FieldName {

        collection_name("Terminal"),
        name("name"),
        cityId("cityId");

        private final String value;
        FieldName(String value) {
            this.value = value;
        }
        public String value() {
            return value;
        }
    }

}
