package com.snapp.buyticket.dal.entity.mongo;

import lombok.Builder;
import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "Trip")
public class Trip extends BaseEntity {
    private ObjectId busId;
    private Long departureDateTime;
    private Long arrivalDateTime;

    private ObjectId formCityId;
    private ObjectId formTerminalId;
    private String formCityName;
    private String formTerminalName;

    private ObjectId toCityId;
    private ObjectId toTerminalId;
    private String toCityName;
    private String toTerminalName;

    private Integer availableCapacity;
    private Long pricePerSeat;

    private Boolean active = true;

    public enum FieldName {

        collection_name("Trip"),
        busId("busId"),
        departureDateTime("departureDateTime"),
        formCityId("formCityId"),
        toCityId("toCityId"),
        formTerminalId("formTerminalId"),
        toTerminalId("toTerminalId"),
        active("active"),
        bus("bus");

        private final String value;
        FieldName(String value) {
            this.value = value;
        }
        public String value() {
            return value;
        }
    }
}
