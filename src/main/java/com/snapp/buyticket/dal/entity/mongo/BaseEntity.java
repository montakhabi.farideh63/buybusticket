package com.snapp.buyticket.dal.entity.mongo;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.*;

import java.io.Serializable;


@Data
public class BaseEntity implements Serializable {

    @Id
    private String id;

    @Version
    protected Long version;

    @CreatedDate
    protected Long createdDate;

    @LastModifiedDate
    protected Long lastModifiedDate;

    @CreatedBy
    private String createdBy;

    @LastModifiedBy
    private String lastModifiedBy;

}
