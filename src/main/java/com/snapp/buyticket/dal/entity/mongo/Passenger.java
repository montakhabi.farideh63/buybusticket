package com.snapp.buyticket.dal.entity.mongo;

import com.snapp.buyticket.enums.Gender;
import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "Passenger")
public class Passenger extends BaseEntity {

    @Indexed
    private String nationalId;
    private String firstName;
    private String lastName;
    private Gender gender;
    private ObjectId userId;

}
