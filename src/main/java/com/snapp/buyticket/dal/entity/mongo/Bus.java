package com.snapp.buyticket.dal.entity.mongo;

import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "Bus")
public class Bus extends BaseEntity{

    private String model;
    private Boolean vip;
    private String numberplate;
    private Long productionDate;
    private Integer capacity;
    private ObjectId busCompanyId;
    private String busCompanyName;

    public enum FieldName {

        collection_name("Bus"),
        vip("vip"),
        busCompanyId("busCompanyId");

        private final String value;
        FieldName(String value) {
            this.value = value;
        }
        public String value() {
            return value;
        }
    }

}
