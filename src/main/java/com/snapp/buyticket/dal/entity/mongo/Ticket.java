package com.snapp.buyticket.dal.entity.mongo;

import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@Document(collection = "Ticket")
public class Ticket extends BaseEntity{

    private List<Seat> seats;
    private List<Passenger> passengers;
    private ObjectId tripId;
    private ObjectId userId;

}
