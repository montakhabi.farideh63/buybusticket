package com.snapp.buyticket.dal.entity.mongo;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "City")
public class City extends BaseEntity {

    private String code;
    private String name;

}
