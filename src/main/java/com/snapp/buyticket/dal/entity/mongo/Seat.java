package com.snapp.buyticket.dal.entity.mongo;

import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "Seat")
public class Seat extends BaseEntity{

    private Integer seatNumber;
    private Boolean available;
    private ObjectId tripId;

}
