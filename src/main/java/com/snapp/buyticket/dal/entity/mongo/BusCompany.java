package com.snapp.buyticket.dal.entity.mongo;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "BusCompany")
public class BusCompany extends BaseEntity{

    private String name;
    private String address;
    private String fax;
    private String email;
    private String phone;
    private String registrationNumber;
}
