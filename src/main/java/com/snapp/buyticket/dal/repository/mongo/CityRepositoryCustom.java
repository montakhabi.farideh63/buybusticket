package com.snapp.buyticket.dal.repository.mongo;

import com.snapp.buyticket.api.dto.search.CityQueryParam;
import com.snapp.buyticket.dal.entity.mongo.City;

import java.util.List;

public interface CityRepositoryCustom {

    List<City> getAllCityByFilter(CityQueryParam queryParam);
}
