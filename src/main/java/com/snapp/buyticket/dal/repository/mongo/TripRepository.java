package com.snapp.buyticket.dal.repository.mongo;

import com.snapp.buyticket.dal.entity.mongo.Trip;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TripRepository extends MongoRepository<Trip,String> , TripRepositoryCustom {

}
