package com.snapp.buyticket.dal.repository.mongo;

import com.snapp.buyticket.dal.entity.mongo.BusCompany;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface BusCompanyRepository extends MongoRepository<BusCompany,String> {
}
