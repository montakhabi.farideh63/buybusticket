package com.snapp.buyticket.dal.repository.mongo;

import com.snapp.buyticket.dal.entity.mongo.Passenger;
import com.snapp.buyticket.dal.entity.mongo.Ticket;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TicketRepository extends MongoRepository<Ticket,String> {
}
