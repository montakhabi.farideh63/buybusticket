package com.snapp.buyticket.dal.repository.mongo;

import com.snapp.buyticket.dal.entity.mongo.Bus;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface BusRepository extends MongoRepository<Bus,String> {


}
