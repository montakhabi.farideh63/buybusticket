package com.snapp.buyticket.dal.repository.mongo;

import com.snapp.buyticket.api.dto.search.TerminalQueryParam;
import com.snapp.buyticket.dal.entity.mongo.Terminal;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface TerminalRepositoryCustom {

    Page<Terminal> getTerminalPageByFilter(TerminalQueryParam queryParam, Pageable pageable);
}
