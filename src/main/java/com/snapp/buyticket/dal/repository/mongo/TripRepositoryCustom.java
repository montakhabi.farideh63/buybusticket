package com.snapp.buyticket.dal.repository.mongo;

import com.snapp.buyticket.dal.model.TripModel;
import com.snapp.buyticket.api.dto.search.TripQueryParam;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface TripRepositoryCustom {

    Page<TripModel> getAllActiveTrips(TripQueryParam tripQueryParamModel, Pageable pageable);
}
