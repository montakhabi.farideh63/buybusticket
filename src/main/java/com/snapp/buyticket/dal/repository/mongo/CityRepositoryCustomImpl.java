package com.snapp.buyticket.dal.repository.mongo;

import com.snapp.buyticket.api.dto.search.CityQueryParam;
import com.snapp.buyticket.api.dto.search.TerminalQueryParam;
import com.snapp.buyticket.dal.entity.mongo.City;
import com.snapp.buyticket.dal.entity.mongo.Terminal;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationOptions;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;

@RequiredArgsConstructor
@Repository
public class CityRepositoryCustomImpl implements CityRepositoryCustom {

    private final MongoTemplate mongoTemplate;

    public List<City> getAllCityByFilter(CityQueryParam queryParam) {
        List<AggregationOperation> aggregationOperations = new ArrayList<>();

        if (!ObjectUtils.isEmpty(queryParam.getName())) {
            Criteria condition = Criteria.where(Terminal.FieldName.name.value()).regex(".*" + queryParam.getName().trim() + ".*", "i");
            aggregationOperations.add(match(condition));
        }
        aggregationOperations.add(Aggregation.project(City.class));
        return mongoTemplate.aggregate(
                newAggregation(aggregationOperations).withOptions(AggregationOptions.builder().allowDiskUse(true).build()),
                City.class,
                City.class).getMappedResults();
    }
}
