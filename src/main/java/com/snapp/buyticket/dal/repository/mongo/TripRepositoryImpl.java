package com.snapp.buyticket.dal.repository.mongo;

import com.snapp.buyticket.dal.entity.mongo.Bus;
import com.snapp.buyticket.dal.entity.mongo.Trip;
import com.snapp.buyticket.api.dto.search.CountResult;
import com.snapp.buyticket.dal.model.TripModel;
import com.snapp.buyticket.api.dto.search.TripQueryParam;
import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.*;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;

@RequiredArgsConstructor
@Repository
public class TripRepositoryImpl implements TripRepositoryCustom {

    private final MongoTemplate mongoTemplate;

    public Page<TripModel> getAllActiveTrips(TripQueryParam tripQueryParamModel, Pageable pageable) {
        List<AggregationOperation> aggregationOperations = new ArrayList<>();
        if (!pageable.getSort().isEmpty())
            aggregationOperations.add(sort(pageable.getSort()));

        LookupOperation lookupOperation = LookupOperation.newLookup()
                .from(Bus.FieldName.collection_name.value())
                .localField(Trip.FieldName.busId.value())
                .foreignField("_id")
                .as(Trip.FieldName.bus.value());

        aggregationOperations.add(lookupOperation);
        aggregationOperations.add(Aggregation.unwind(Trip.FieldName.bus.value())) ;

        Criteria condition = getCriteriaByTripQueryParamModel(tripQueryParamModel);
        aggregationOperations.add(match(condition));
        aggregationOperations.add(project(TripModel.class));
        aggregationOperations.add(Aggregation.skip((long) pageable.getPageNumber() * pageable.getPageSize()));
        aggregationOperations.add(Aggregation.limit(pageable.getPageSize()));

        List<TripModel> results = mongoTemplate.aggregate(
                newAggregation(aggregationOperations).withOptions(AggregationOptions.builder().allowDiskUse(true).build()),
                Trip.class,
                TripModel.class).getMappedResults();

        long total = getCountOfTrips(aggregationOperations);
        return new PageImpl<>(results, pageable, total);

    }

    private Criteria getCriteriaByTripQueryParamModel(TripQueryParam queryParamModel) {
        List<Criteria> criteriaList = new ArrayList<>();

        if (!ObjectUtils.isEmpty(queryParamModel.getDepartureDateTime())) {
            criteriaList.add(Criteria.where(Trip.FieldName.departureDateTime.value()).is(queryParamModel.getDepartureDateTime()));
        }

        if (!ObjectUtils.isEmpty(queryParamModel.getFormCityId())) {
            criteriaList.add(Criteria.where(Trip.FieldName.formCityId.value()).is(new ObjectId(queryParamModel.getFormCityId())));
        }

        if (!ObjectUtils.isEmpty(queryParamModel.getToCityId())) {
            criteriaList.add(Criteria.where(Trip.FieldName.toCityId.value()).is(new ObjectId(queryParamModel.getToCityId())));
        }

        if (!ObjectUtils.isEmpty(queryParamModel.getFormTerminalId())) {
            criteriaList.add(Criteria.where(Trip.FieldName.formTerminalId.value()).is(new ObjectId(queryParamModel.getFormTerminalId())));
        }

        if (!ObjectUtils.isEmpty(queryParamModel.getToTerminalId())) {
            criteriaList.add(Criteria.where(Trip.FieldName.toTerminalId.value()).is(new ObjectId(queryParamModel.getToTerminalId())));
        }

        if (!ObjectUtils.isEmpty(queryParamModel.getBusCompanyId())) {
            criteriaList.add(Criteria.where(Trip.FieldName.bus.value() + "." + Bus.FieldName.vip.value()).is(queryParamModel.getBusVip()));
        }

        if (!ObjectUtils.isEmpty(queryParamModel.getBusCompanyId())) {
            criteriaList.add(Criteria.where(Trip.FieldName.bus.value() + "." + Bus.FieldName.busCompanyId.value()).is(new ObjectId(queryParamModel.getBusCompanyId())));
        }

        criteriaList.add(Criteria.where(Trip.FieldName.active.value()).is(true));
        return new Criteria().andOperator(criteriaList);
    }

    private long getCountOfTrips(List<AggregationOperation> aggregationOperations) {
        aggregationOperations.add(Aggregation.count().as("count"));
        AggregationResults<CountResult> result = mongoTemplate.aggregate(Aggregation.newAggregation(aggregationOperations), Trip.class, CountResult.class);
        CountResult countResult = result.getUniqueMappedResult();
        return countResult != null ? countResult.getCount() : 0;
    }
}
