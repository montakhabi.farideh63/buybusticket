package com.snapp.buyticket.dal.repository.mongo;

import com.snapp.buyticket.dal.entity.mongo.Seat;
import com.snapp.usermanagment.dal.entity.mongo.User;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface SeatRepository extends MongoRepository<Seat,String> {

    List<Seat> findByTripId(ObjectId tripId);
}
