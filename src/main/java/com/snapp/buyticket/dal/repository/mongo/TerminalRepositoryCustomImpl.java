package com.snapp.buyticket.dal.repository.mongo;

import com.snapp.buyticket.api.dto.search.TerminalQueryParam;
import com.snapp.buyticket.dal.entity.mongo.Terminal;
import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationOptions;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;

@RequiredArgsConstructor
@Repository
public class TerminalRepositoryCustomImpl implements TerminalRepositoryCustom {

    private final MongoTemplate mongoTemplate;

    public Page<Terminal> getTerminalPageByFilter(TerminalQueryParam queryParam, Pageable pageable) {
        List<AggregationOperation> aggregationOperations = new ArrayList<>();

        if (!pageable.getSort().isEmpty())
            aggregationOperations.add(sort(pageable.getSort()));

        Criteria condition = getCriteriaByQueryParam(queryParam);
        if (condition != null)
            aggregationOperations.add(match(condition));

        aggregationOperations.add(Aggregation.project(Terminal.class));
        aggregationOperations.add(Aggregation.skip((long) pageable.getPageNumber() * pageable.getPageSize()));
        aggregationOperations.add(Aggregation.limit(pageable.getPageSize()));

        List<Terminal> results = mongoTemplate.aggregate(
                newAggregation(aggregationOperations).withOptions(AggregationOptions.builder().allowDiskUse(true).build()),
                Terminal.class,
                Terminal.class).getMappedResults();

        Query query = condition == null ? new Query() : new Query(condition);
        long total = mongoTemplate.count(query, Terminal.class);
        return new PageImpl<>(results, pageable, total);
    }

    private Criteria getCriteriaByQueryParam(TerminalQueryParam queryParam) {
        List<Criteria> criteriaList = new ArrayList<>();

        if (!ObjectUtils.isEmpty(queryParam.getCityId())) {
            criteriaList.add(Criteria.where(Terminal.FieldName.cityId.value()).is(new ObjectId(queryParam.getCityId().trim())));
        }

        if (!ObjectUtils.isEmpty(queryParam.getName())) {
            criteriaList.add(Criteria.where(Terminal.FieldName.name.value()).regex(".*" + queryParam.getName().trim() + ".*", "i"));
        }
        return criteriaList.isEmpty() ? null : new Criteria().andOperator(criteriaList);
    }

}
