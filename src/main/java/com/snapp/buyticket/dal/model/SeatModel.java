package com.snapp.buyticket.dal.model;

import lombok.Data;
import org.bson.types.ObjectId;

@Data
public class SeatModel {

    private String id;
    private Integer seatNumber;
    private Boolean available;
    private ObjectId tripId;
}
