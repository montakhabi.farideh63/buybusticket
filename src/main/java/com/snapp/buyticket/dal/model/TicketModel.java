package com.snapp.buyticket.dal.model;

import com.snapp.buyticket.dal.entity.mongo.Passenger;
import com.snapp.buyticket.dal.entity.mongo.Seat;
import lombok.Data;
import org.bson.types.ObjectId;

import java.util.List;

@Data
public class TicketModel {

    private String id;
    private List<SeatModel> seats;
    private List<PassengerModel> passengers;
    private ObjectId tripId;
    private ObjectId userId;
}
