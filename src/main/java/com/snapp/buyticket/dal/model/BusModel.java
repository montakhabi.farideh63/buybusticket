package com.snapp.buyticket.dal.model;

import lombok.Data;
import org.bson.types.ObjectId;


@Data
public class BusModel {

    private String id;
    private String model;
    private Boolean vip;
    private String numberplate;
    private Long productionDate;
    private Integer capacity;
    private ObjectId busCompanyId;
    private String busCompanyName;

}
