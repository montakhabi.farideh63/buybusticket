package com.snapp.buyticket.dal.model;

import lombok.Data;

@Data
public class BusCompanyModel {

    private String id;
    private String name;
    private String address;
    private String fax;
    private String email;
    private String phone;
    private String registrationNumber;
}
