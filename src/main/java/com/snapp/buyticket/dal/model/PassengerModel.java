package com.snapp.buyticket.dal.model;

import com.snapp.buyticket.enums.Gender;
import lombok.Data;
import org.bson.types.ObjectId;

@Data
public class PassengerModel {

    private String id;
    private String nationalId;
    private String firstName;
    private String lastName;
    private Gender gender;
    private ObjectId userId;
}
