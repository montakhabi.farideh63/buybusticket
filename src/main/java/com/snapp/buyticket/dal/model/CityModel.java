package com.snapp.buyticket.dal.model;

import lombok.Data;

@Data
public class CityModel {

    private String id;
    private String code;
    private String name;
}
