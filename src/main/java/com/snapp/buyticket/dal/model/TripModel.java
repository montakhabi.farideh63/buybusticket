package com.snapp.buyticket.dal.model;

import lombok.Data;
import org.bson.types.ObjectId;

@Data
public class TripModel {

    private String id;
    private ObjectId busId;
    private BusModel bus;
    private Long departureDateTime;
    private Long arrivalDateTime;
    private ObjectId formCityId;
    private ObjectId formTerminalId;
    private String formCityName;
    private String formTerminalName;
    private ObjectId toCityId;
    private ObjectId toTerminalId;
    private String toCityName;
    private String toTerminalName;
    private Integer availableCapacity;
    private Long pricePerSeat;
    private Boolean active;
}
