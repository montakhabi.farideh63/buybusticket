package com.snapp.buyticket.dal.model;

import lombok.Data;
import org.bson.types.ObjectId;

@Data
public class TerminalModel {

    private String id;
    private String name;
    private ObjectId cityId;
    private String cityName;
    private String phone;
}
