
package com.snapp.buyticket.enums;

public enum Gender {

    MALE("MALE","مرد"),
    FEMALE("FEMALE","زن");

    private final String key;
    private final String text;

    Gender(String key, String text) {
        this.key = key;
        this.text = text;
    }

    public String getKey() {
        return this.key;
    }

    public String getText() {
        return this.text;
    }
}