package com.snapp.buyticket.service;

import com.snapp.buyticket.dal.model.PassengerModel;

import java.util.List;

public interface PassengerService {

    String save(PassengerModel model);

    List<PassengerModel> saveList(List<PassengerModel> passengerModelList);
}
