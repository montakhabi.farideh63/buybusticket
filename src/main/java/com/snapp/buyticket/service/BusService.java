package com.snapp.buyticket.service;

import com.snapp.buyticket.dal.model.BusModel;

public interface BusService {

    String save(BusModel model);

    BusModel findById(String id);
}
