package com.snapp.buyticket.service;

import com.snapp.buyticket.dal.model.TicketModel;

public interface TicketService {

    String save(TicketModel model);

    TicketModel findById(String id);
}
