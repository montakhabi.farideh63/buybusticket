package com.snapp.buyticket.service;

import com.snapp.buyticket.api.dto.search.CityQueryParam;
import com.snapp.buyticket.api.dto.search.TripQueryParam;
import com.snapp.buyticket.api.handler.exception.NotFoundException;
import com.snapp.buyticket.dal.entity.mongo.City;
import com.snapp.buyticket.dal.model.CityModel;
import com.snapp.buyticket.dal.model.TripModel;
import com.snapp.buyticket.dal.repository.mongo.CityRepository;
import com.snapp.buyticket.service.mapper.ModelEntityMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class CityServiceImpl implements CityService {

    private final ModelEntityMapper mapper;
    private final CityRepository cityRepository;

    public List<CityModel> getAllCity(CityQueryParam cityQueryParam) {
        return cityRepository.getAllCityByFilter(cityQueryParam).stream().map(mapper::getModelFromEntity).toList();
    }

    public String save(CityModel model){
        City newEntity = cityRepository.save(mapper.getEntityFromModel(model));
        return newEntity.getId();
    }

    public CityModel findById(String id){
        City loadedCity = cityRepository.findById(id).orElseThrow(() -> new NotFoundException("city.not.found"));
        return mapper.getModelFromEntity(loadedCity);
    }
}
