package com.snapp.buyticket.service;

import com.snapp.buyticket.api.dto.search.TerminalQueryParam;
import com.snapp.buyticket.dal.model.TerminalModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface TerminalService {

    Page<TerminalModel> getTerminalPageByFilter(TerminalQueryParam queryParam, Pageable pageable);

    String save(TerminalModel model);

    TerminalModel findById(String id);
}
