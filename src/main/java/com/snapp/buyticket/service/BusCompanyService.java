package com.snapp.buyticket.service;

import com.snapp.buyticket.dal.model.BusCompanyModel;

public interface BusCompanyService {

    String save(BusCompanyModel model);
    BusCompanyModel findById(String id);
}
