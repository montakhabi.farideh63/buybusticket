package com.snapp.buyticket.service;


import com.snapp.buyticket.dal.model.TripModel;
import com.snapp.buyticket.api.dto.search.TripQueryParam;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface TripService {

    String save(TripModel model);

    TripModel findById(String id);

    Page<TripModel> getAllActiveTrips(TripQueryParam tripQueryParam, Pageable pageable);

    void updateAvailableCapacity(String tripId, int numberOfSeat);
}
