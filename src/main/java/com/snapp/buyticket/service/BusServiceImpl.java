package com.snapp.buyticket.service;

import com.snapp.buyticket.api.handler.exception.NotFoundException;
import com.snapp.buyticket.dal.entity.mongo.Bus;
import com.snapp.buyticket.dal.entity.mongo.City;
import com.snapp.buyticket.dal.model.BusModel;
import com.snapp.buyticket.dal.model.CityModel;
import com.snapp.buyticket.dal.repository.mongo.BusRepository;
import com.snapp.buyticket.service.mapper.ModelEntityMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class BusServiceImpl implements BusService {

    private final ModelEntityMapper mapper;
    private final BusRepository busRepository;

    public String save(BusModel model){
      Bus newEntity = busRepository.save(mapper.getEntityFromModel(model));
      return newEntity.getId();
    }

  public BusModel findById(String id){
    Bus loadedBus = busRepository.findById(id).orElseThrow(() -> new NotFoundException("bus.not.found"));
    return mapper.getModelFromEntity(loadedBus);
  }

}
