package com.snapp.buyticket.service;

import com.snapp.buyticket.dal.entity.mongo.Passenger;
import com.snapp.buyticket.dal.model.PassengerModel;
import com.snapp.buyticket.dal.repository.mongo.PassengerRepository;
import com.snapp.buyticket.service.mapper.ModelEntityMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class PassengerServiceImpl implements PassengerService{

    private final PassengerRepository passengerRepository;
    private final ModelEntityMapper mapper;

    public String save(PassengerModel model){
        Passenger newEntity = passengerRepository.save(mapper.getEntityFromModel(model));
        return newEntity.getId();
    }

    public List<PassengerModel> saveList(List<PassengerModel> passengerModelList){
        List<Passenger> passengerList =  passengerRepository.saveAll(passengerModelList.stream().map(mapper::getEntityFromModel).toList());
        return passengerList.stream().map(mapper::getModelFromEntity).toList();
    }
}
