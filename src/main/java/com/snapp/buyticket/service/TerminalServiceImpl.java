package com.snapp.buyticket.service;

import com.snapp.buyticket.api.dto.search.TerminalQueryParam;
import com.snapp.buyticket.api.handler.exception.NotFoundException;
import com.snapp.buyticket.dal.entity.mongo.Terminal;
import com.snapp.buyticket.dal.model.CityModel;
import com.snapp.buyticket.dal.model.TerminalModel;
import com.snapp.buyticket.dal.repository.mongo.TerminalRepository;
import com.snapp.buyticket.service.mapper.ModelEntityMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

@Service
@RequiredArgsConstructor
@Slf4j
public class TerminalServiceImpl implements TerminalService {

    private final ModelEntityMapper mapper;
    private final TerminalRepository terminalRepository;

    public Page<TerminalModel> getTerminalPageByFilter(TerminalQueryParam queryParam, Pageable pageable) {
        return terminalRepository.getTerminalPageByFilter(queryParam, pageable).map(mapper::getModelFromEntity);
    }

    public String save(TerminalModel model){
        Terminal newEntity = terminalRepository.save(mapper.getEntityFromModel(model));
        return newEntity.getId();
    }

    public TerminalModel findById(String id){
        Terminal loadedTerminal = terminalRepository.findById(id).orElseThrow(() -> new NotFoundException("terminal.not.found"));
        return mapper.getModelFromEntity(loadedTerminal);
    }
}
