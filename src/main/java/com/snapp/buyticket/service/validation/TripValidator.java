package com.snapp.buyticket.service.validation;

import com.snapp.buyticket.dal.model.TripModel;

public interface TripValidator {

    void validation(TripModel tripModel);
}
