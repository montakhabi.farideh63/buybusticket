package com.snapp.buyticket.service.validation;

import com.snapp.buyticket.api.handler.exception.InvalidDataException;
import com.snapp.buyticket.dal.model.SeatModel;
import com.snapp.buyticket.dal.model.TicketModel;
import com.snapp.buyticket.dal.model.TripModel;
import com.snapp.buyticket.service.SeatService;
import com.snapp.buyticket.service.TripService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class TicketValidatorImpl implements TicketValidator{

    private final TripService tripService;
    private final SeatService seatService;

    public void validation(TicketModel ticketModel) {
        tripValidation(ticketModel);
        seatValidation(ticketModel);
    }

    private void tripValidation(TicketModel ticketModel) {
        TripModel tripModel =  tripService.findById(ticketModel.getTripId().toString());
        if(!tripModel.getActive()){
            throw new InvalidDataException("{trip.is.not.active}");
        }
        if(tripModel.getAvailableCapacity() < ticketModel.getPassengers().size()){
            throw new InvalidDataException("{trip.available.capacity.is.less.than.number.passengers}");
        }
    }

    private void seatValidation(TicketModel ticketModel) {
        TripModel tripModel = tripService.findById(ticketModel.getTripId().toString());
        if (ticketModel.getSeats().size() != ticketModel.getPassengers().size()) {
            throw new InvalidDataException("{trip.number.seats.is.not.equal.number.passengers}");
        }

        ticketModel.getSeats().forEach(seat -> {
            SeatModel seatModel = seatService.findById(seat.getId());
            if (!seatModel.getAvailable()) {
                throw new InvalidDataException("{seat.is.not.available}");
            }

            if (!seatModel.getTripId().toString().equals(tripModel.getId())) {
                throw new InvalidDataException("{seat.does.not.belong.trip}");
            }
        });
    }
}
