package com.snapp.buyticket.service.validation;


import com.snapp.buyticket.dal.model.TicketModel;

public interface TicketValidator {
    void validation(TicketModel ticketModel);

}
