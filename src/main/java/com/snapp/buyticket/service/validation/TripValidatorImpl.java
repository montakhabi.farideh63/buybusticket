package com.snapp.buyticket.service.validation;

import com.snapp.buyticket.api.handler.exception.InvalidDataException;
import com.snapp.buyticket.dal.model.BusModel;
import com.snapp.buyticket.dal.model.CityModel;
import com.snapp.buyticket.dal.model.TripModel;
import com.snapp.buyticket.service.BusService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class TripValidatorImpl implements TripValidator {

    private final BusService busService;

    public void validation(TripModel tripModel) {
        busValidation(tripModel);
        terminalValidation(tripModel);
        dateTimeValidation(tripModel);
    }

    private void busValidation(TripModel tripModel) {
        BusModel busModel = busService.findById(tripModel.getBusId().toString());
        if(tripModel.getAvailableCapacity() > busModel.getCapacity()){
            throw new InvalidDataException("{availableCapacity.can.not.be.lager.bus.capacity}");
        }
    }

    private void dateTimeValidation(TripModel tripModel) {
        //TODO
    }

    private void terminalValidation(TripModel tripModel) {
        //TODO
    }


}
