package com.snapp.buyticket.service;

import com.snapp.buyticket.api.handler.exception.NotFoundException;
import com.snapp.buyticket.dal.entity.mongo.BusCompany;
import com.snapp.buyticket.dal.model.BusCompanyModel;
import com.snapp.buyticket.dal.repository.mongo.BusCompanyRepository;
import com.snapp.buyticket.service.mapper.ModelEntityMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class BusCompanyServiceImpl implements BusCompanyService{

    private final ModelEntityMapper mapper;
    private final BusCompanyRepository busCompanyRepository;

    public String save(BusCompanyModel model){
        BusCompany newEntity = busCompanyRepository.save(mapper.getEntityFromModel(model));
        return newEntity.getId();
    }

    public BusCompanyModel findById(String id){
        BusCompany loadedBusCompany = busCompanyRepository.findById(id).orElseThrow(() -> new NotFoundException("bus.company.not.found"));
        return mapper.getModelFromEntity(loadedBusCompany);
    }
}
