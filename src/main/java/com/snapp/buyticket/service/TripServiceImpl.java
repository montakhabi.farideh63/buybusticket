package com.snapp.buyticket.service;

import com.snapp.buyticket.api.handler.exception.NotFoundException;
import com.snapp.buyticket.dal.entity.mongo.City;
import com.snapp.buyticket.dal.entity.mongo.Trip;
import com.snapp.buyticket.dal.model.CityModel;
import com.snapp.buyticket.dal.model.TerminalModel;
import com.snapp.buyticket.dal.model.TripModel;
import com.snapp.buyticket.api.dto.search.TripQueryParam;
import com.snapp.buyticket.dal.repository.mongo.TripRepository;
import com.snapp.buyticket.service.validation.TripValidator;
import com.snapp.buyticket.service.mapper.ModelEntityMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class TripServiceImpl implements TripService {

    private final TripRepository tripRepository;
    private final TerminalService terminalService;
    private final TripValidator tripValidator;
    private final ModelEntityMapper mapper;

    public String save(TripModel model){

        TerminalModel formTerminalModel = terminalService.findById(model.getFormTerminalId().toString());
        model.setFormTerminalName(formTerminalModel.getName());
        model.setFormCityId(formTerminalModel.getCityId());
        model.setFormCityName(formTerminalModel.getCityName());

        TerminalModel toTerminalModel = terminalService.findById(model.getToTerminalId().toString());
        model.setToTerminalName(toTerminalModel.getName());
        model.setToCityId(toTerminalModel.getCityId());
        model.setToCityName(toTerminalModel.getCityName());

        tripValidator.validation(model);//TODO
        Trip newEntity = tripRepository.save(mapper.getEntityFromModel(model));
        return newEntity.getId();
    }


    public Page<TripModel> getAllActiveTrips(TripQueryParam tripQueryParam,
                                             Pageable pageable) {
        return tripRepository.getAllActiveTrips(tripQueryParam, pageable);
    }

    public void updateAvailableCapacity(String id, int numberOfSeat) {
        Trip loadedTrip = tripRepository.findById(id).orElseThrow(() -> new NotFoundException("trip.not.found"));
        loadedTrip.setAvailableCapacity(loadedTrip.getAvailableCapacity() - numberOfSeat);
        tripRepository.save(loadedTrip);
    }

    public TripModel findById(String id){
        Trip loadedTrip = tripRepository.findById(id).orElseThrow(() -> new NotFoundException("trip.not.found"));
        return mapper.getModelFromEntity(loadedTrip);
    }
}
