package com.snapp.buyticket.service;

import com.snapp.buyticket.api.handler.exception.NotFoundException;
import com.snapp.buyticket.dal.entity.mongo.Seat;
import com.snapp.buyticket.dal.model.SeatModel;
import com.snapp.buyticket.dal.repository.mongo.SeatRepository;
import com.snapp.buyticket.service.mapper.ModelEntityMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class SeatServiceImpl implements SeatService {

    private final SeatRepository seatRepository;
    private final ModelEntityMapper mapper;

    public String save(SeatModel model){
        Seat newEntity = seatRepository.save(mapper.getEntityFromModel(model));
        return newEntity.getId();
    }

    public List<SeatModel> saveList(List<SeatModel> seatModelList){
        List<Seat> seatList =  seatRepository.saveAll(seatModelList.stream().map(mapper::getEntityFromModel).toList());
        return seatList.stream().map(mapper::getModelFromEntity).toList();
    }

    public SeatModel findById(String id){
        Seat loadedSeat = seatRepository.findById(id).orElseThrow(() -> new NotFoundException("seat.not.found"));
        return mapper.getModelFromEntity(loadedSeat);
    }

    public void updateAvailableStatus(List<SeatModel> seats) {
        seats.forEach( s ->{
            Seat loadedSeat = seatRepository.findById(s.getId()).orElseThrow(() -> new NotFoundException("seat.not.found"));
            loadedSeat.setAvailable(false);
            seatRepository.save(loadedSeat);
        });
    }

    public List<SeatModel> getSeatsByTripId(String tripId){
        return seatRepository.findByTripId(new ObjectId(tripId)).stream().map(mapper::getModelFromEntity).toList();
    }
}
