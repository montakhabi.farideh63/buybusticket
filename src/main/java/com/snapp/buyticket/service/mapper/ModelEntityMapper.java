package com.snapp.buyticket.service.mapper;

import com.snapp.buyticket.dal.entity.mongo.*;
import com.snapp.buyticket.dal.model.*;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ModelEntityMapper {

    BusModel getModelFromEntity(Bus entity);
    Bus getEntityFromModel(BusModel model);

    BusCompanyModel getModelFromEntity(BusCompany entity);
    BusCompany getEntityFromModel(BusCompanyModel model);

    CityModel getModelFromEntity(City entity);
    City getEntityFromModel(CityModel model);

    TerminalModel getModelFromEntity(Terminal entity);
    Terminal getEntityFromModel(TerminalModel model);

    TripModel getModelFromEntity(Trip entity);
    Trip getEntityFromModel(TripModel model);

    SeatModel getModelFromEntity(Seat entity);
    Seat getEntityFromModel(SeatModel model);

    PassengerModel getModelFromEntity(Passenger entity);
    Passenger getEntityFromModel(PassengerModel model);

    TicketModel getModelFromEntity(Ticket entity);
    Ticket getEntityFromModel(TicketModel model);

}
