package com.snapp.buyticket.service;

import com.snapp.buyticket.api.dto.search.CityQueryParam;
import com.snapp.buyticket.dal.model.CityModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface CityService {

    List<CityModel> getAllCity(CityQueryParam cityQueryParam);

    String save(CityModel model);

    CityModel findById(String id);
}
