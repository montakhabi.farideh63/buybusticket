package com.snapp.buyticket.service;

import com.snapp.buyticket.api.handler.exception.NotFoundException;
import com.snapp.buyticket.dal.entity.mongo.Ticket;
import com.snapp.buyticket.dal.entity.mongo.Ticket;
import com.snapp.buyticket.dal.model.TicketModel;
import com.snapp.buyticket.dal.model.TicketModel;
import com.snapp.buyticket.dal.repository.mongo.TicketRepository;
import com.snapp.buyticket.service.mapper.ModelEntityMapper;
import com.snapp.buyticket.service.validation.TicketValidator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class TicketServiceImpl implements TicketService {

    private final TicketValidator ticketValidator;
    private final TicketRepository tripRepository;
    private final ModelEntityMapper mapper;

    public String save(TicketModel model){
        ticketValidator.validation(model);//TODO
        //TODO for call api from payment microservice and save transaction data
        Ticket newEntity = tripRepository.save(mapper.getEntityFromModel(model));
        return newEntity.getId();
    }

    public TicketModel findById(String id){
        Ticket loadedTicket = tripRepository.findById(id).orElseThrow(() -> new NotFoundException("ticket.not.found"));
        return mapper.getModelFromEntity(loadedTicket);
    }
}
