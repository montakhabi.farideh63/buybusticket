package com.snapp.buyticket.service;

import com.snapp.buyticket.dal.model.SeatModel;

import java.util.List;

public interface SeatService {

    String save(SeatModel model);

    List<SeatModel> saveList(List<SeatModel> seatModelList);

    void updateAvailableStatus(List<SeatModel> seats);

    SeatModel findById(String id);

    List<SeatModel> getSeatsByTripId(String tripId);
}
