package com.snapp.buyticket.api.handler.exception;

import org.springframework.http.HttpStatus;

public class AuthenticationException extends CommonException {

    private static final HttpStatus HTTP_STATUS = HttpStatus.UNAUTHORIZED;
    private static final String DEFAULT_REASON = "{common.error.authentication}";

    public AuthenticationException() {
        super(HTTP_STATUS);
    }

    public AuthenticationException(String reason) {
        super(HTTP_STATUS,reason);
    }

    @Override
    public String getDefaultReason() {
        return "{DEFAULT_REASON}";
    }

}
