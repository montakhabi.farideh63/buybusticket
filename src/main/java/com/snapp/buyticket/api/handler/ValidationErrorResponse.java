package com.snapp.buyticket.api.handler;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@Data
public class ValidationErrorResponse {
    private int code;
    private String msg;
    private List<Violation> violations = new ArrayList<>();

}
