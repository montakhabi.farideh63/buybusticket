package com.snapp.buyticket.api.handler.exception;

import org.springframework.http.HttpStatus;

public class AlreadyRegisterException extends CommonException {
    private static final HttpStatus HTTP_STATUS = HttpStatus.NOT_ACCEPTABLE;
    private static final String DEFAULT_REASON = "{common.error.already.register.user}";

    public AlreadyRegisterException() {
        super(HTTP_STATUS);
    }

    public AlreadyRegisterException(String reason) {
        super(HTTP_STATUS,reason);
    }

    @Override
    public String getDefaultReason() {
        return "{DEFAULT_REASON}";
    }
}
