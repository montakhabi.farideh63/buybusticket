package com.snapp.buyticket.api.handler.exception;

import org.springframework.http.HttpStatus;

public class InvalidDataException extends CommonException {
    private static final HttpStatus HTTP_STATUS = HttpStatus.NOT_ACCEPTABLE;
    private static final String DEFAULT_REASON = "{common.error.invalid.data}";

    public InvalidDataException() {
        super(HTTP_STATUS);
    }

    @Override
    public String getDefaultReason() {
        return DEFAULT_REASON;
    }

    public InvalidDataException(String reason) {
        super(HTTP_STATUS, reason);
    }

    public InvalidDataException(Throwable cause) {
        super(HTTP_STATUS, DEFAULT_REASON, cause);
    }

    public InvalidDataException(String reason, Throwable cause) {
        super(HTTP_STATUS, reason, cause);
    }

    public InvalidDataException(int code) {
        super(HTTP_STATUS, code);
    }

    public InvalidDataException(String reason, int code) {
        super(HTTP_STATUS, reason, code);
    }
}
