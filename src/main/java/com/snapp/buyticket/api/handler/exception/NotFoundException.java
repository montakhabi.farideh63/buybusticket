package com.snapp.buyticket.api.handler.exception;

import org.springframework.http.HttpStatus;

public class NotFoundException extends CommonException {
    private static final HttpStatus HTTP_STATUS = HttpStatus.NOT_FOUND;
    private static final String DEFAULT_REASON = "{common.error.notfound}";

    public NotFoundException() {
        super(HTTP_STATUS);
    }

    @Override
    public String getDefaultReason() {
        return DEFAULT_REASON;
    }

    public NotFoundException(String reason) {
        super(HTTP_STATUS, reason);
    }

    public NotFoundException(Throwable cause) {
        super(HTTP_STATUS, DEFAULT_REASON, cause);
    }

    public NotFoundException(String reason, Throwable cause) {
        super(HTTP_STATUS, reason, cause);
    }

    public NotFoundException(int code) {
        super(HTTP_STATUS, code);
    }

    public NotFoundException(String reason, int code) {
        super(HTTP_STATUS, reason, code);
    }

}
