package com.snapp.buyticket.api.handler.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.util.ObjectUtils;
import org.springframework.web.server.ResponseStatusException;

@Getter
public abstract class CommonException extends ResponseStatusException {

    private int code;

    public abstract String getDefaultReason();

    public CommonException(HttpStatus status) {
        super(status);
    }

    public CommonException(HttpStatus status, String reason) {
        super(status, reason);
    }

    public CommonException(HttpStatus status,int code) {
        super(status);
        this.code = code;
    }

    public CommonException(HttpStatus status, String reason, int code) {
        super(status, reason);
        this.code = code;
    }

    public CommonException(HttpStatus status, String reason, Throwable cause) {
        super(status, reason, cause);
    }

    @Override
    public String getReason() {
        String reason = super.getReason();
        if (ObjectUtils.isEmpty(reason))
            reason = getDefaultReason();
        return reason;
    }
}
