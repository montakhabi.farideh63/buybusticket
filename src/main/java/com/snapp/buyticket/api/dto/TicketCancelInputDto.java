package com.snapp.buyticket.api.dto;

import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

import java.util.List;

@Data
public class TicketCancelInputDto {

    @NotEmpty(message = "seats.is.empty")
    private List<String> seatIds;

    @NotEmpty(message = "passengers.is.empty")
    private List<String> passengerIds;
}
