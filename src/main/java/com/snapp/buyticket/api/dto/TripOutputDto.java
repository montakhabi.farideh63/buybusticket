package com.snapp.buyticket.api.dto;

import com.snapp.buyticket.dal.model.BusModel;
import lombok.Data;
import org.bson.types.ObjectId;

@Data
public class TripOutputDto {

    private String id;
    private BusOutputDto bus;
    private Long departureDateTime;
    private String formCityName;
    private String formTerminalName;
    private String toCityName;
    private String toTerminalName;
    private Integer availableCapacity;
    private Long pricePerSeat;
}
