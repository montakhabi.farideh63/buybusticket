package com.snapp.buyticket.api.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import lombok.Data;
import org.bson.types.ObjectId;

import java.util.List;

@Data
public class TicketInputDto {

    @NotBlank(message = "tripId.is.blank")
    private String tripId;

    @NotEmpty(message = "seats.is.empty")
    private List<TicketSeatInputDto> seats;

    @NotEmpty(message = "passengers.is.empty")
    private List<PassengerInputDto> passengers;

    @NotBlank(message = "buyerMobile.is.blank")
    private String buyerMobile;

}
