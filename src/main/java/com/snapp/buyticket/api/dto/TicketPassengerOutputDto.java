package com.snapp.buyticket.api.dto;

import com.snapp.buyticket.enums.Gender;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class TicketPassengerOutputDto {

    private String firstName;
    private String lastName;
}
