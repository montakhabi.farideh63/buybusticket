package com.snapp.buyticket.api.dto.search;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.Objects;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageQueryParam {
    private Integer pageNumber = 0;
    private Integer pageSize = 10;
    private Sort sort;
    private Set<String> sortKey;

    public Pageable convertPageQueryParamToPageable() {
        String[] queryParams = (Objects.isNull(this.getSortKey()) || (this.getSortKey().size() == 0)) ? null : this.getSortKey().toArray(new String[0]);
        String sortBy = Objects.isNull(this.getSort()) ? null : this.getSort().name();
        if (!Objects.isNull(queryParams) && !Objects.isNull(sortBy))
            return PageRequest.of(
                    this.getPageNumber(),
                    this.getPageSize(),
                    org.springframework.data.domain.Sort.Direction.fromString(sortBy),
                    queryParams
            );
        else
            return PageRequest.of(
                    this.getPageNumber(),
                    this.getPageSize()
            );
    }

    private enum Sort {
        ASC,
        DESC
    }
}
