package com.snapp.buyticket.api.dto;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import org.bson.types.ObjectId;

@Data
public class TerminalOutputDto {

    private String id;
    private String name;
    private ObjectId cityId;
    private String cityName;
    private String phone;
}
