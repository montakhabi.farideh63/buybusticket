package com.snapp.buyticket.api.dto;

import com.snapp.buyticket.enums.Gender;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.data.mongodb.core.index.Indexed;

@Data
public class PassengerInputDto {

    private String id;

    @NotBlank(message = "nationalId.is.blank")
    private String nationalId;

    @NotBlank(message = "firstName.is.blank")
    private String firstName;

    @NotBlank(message = "lastName.is.blank")
    private String lastName;

    @NotNull(message="gender.is.null")
    private Gender gender;
}
