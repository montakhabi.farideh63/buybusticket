package com.snapp.buyticket.api.dto;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class BusCompanyInputDto {

    @NotBlank(message = "name.is.blank")
    private String name;

    private String address;

    private String fax;

    private String email;

    private String phone;

    @NotBlank(message = "registrationNumber.is.blank")
    private String registrationNumber;
}
