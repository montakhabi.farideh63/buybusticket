package com.snapp.buyticket.api.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.Data;
import java.util.List;

@Data
public class TripInputDto {

    @NotBlank(message = "busId.is.blank")
    private String busId;

    @NotNull(message="departureDateTime.is.null")
    private Long departureDateTime;

    @NotNull(message="arrivalDateTime.is.null")
    private Long arrivalDateTime;

    @NotBlank(message = "formTerminalId.is.blank")
    private String formTerminalId;

    @NotBlank(message = "toTerminalId.is.blank")
    private String toTerminalId;

    @NotNull(message="pricePerSeat.is.null")
    @Positive(message = "pricePerSeat.not.valid")
    private Long pricePerSeat;

    @NotEmpty(message = "seats.is.empty")
    private List<SeatInputDto> seats;
}
