package com.snapp.buyticket.api.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class TicketSeatInputDto {

    @NotBlank(message="seatId.is.null")
    private String seatId;

    @NotNull(message="seatNumber.is.null")
    private Integer seatNumber;
}
