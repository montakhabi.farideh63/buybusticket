package com.snapp.buyticket.api.dto;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class CityOutputDto {

    private String id;
    private String name;
}
