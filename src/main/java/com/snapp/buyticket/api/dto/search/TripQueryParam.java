package com.snapp.buyticket.api.dto.search;

import lombok.Data;

@Data
public class TripQueryParam {

    private Boolean busVip;
    private String busCompanyId;
    private Long departureDateTime;
    private String formCityId;
    private String formTerminalId;
    private String toCityId;
    private String toTerminalId;
}
