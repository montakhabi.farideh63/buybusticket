package com.snapp.buyticket.api.dto.search;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class TerminalQueryParam {

    private String name;
    private String cityId;
}
