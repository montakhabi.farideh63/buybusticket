package com.snapp.buyticket.api.dto;

import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class SeatInputDto {

    @NotNull(message="seatNumber.is.null")
    private Integer seatNumber;

    @NotNull(message="available.is.null")
    private Boolean available;
}
