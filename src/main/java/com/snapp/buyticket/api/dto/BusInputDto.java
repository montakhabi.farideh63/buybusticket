package com.snapp.buyticket.api.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
public class BusInputDto {

    @NotBlank(message = "model.is.blank")
    private String model;

    @NotNull(message = "vip.is.blank")
    private Boolean vip;

    @NotBlank(message = "numberplate.is.blank")
    private String numberplate;

    private Long productionDate;

    @NotNull(message="capacity.is.null")
    @Positive(message = "capacity.not.valid")
    private Integer capacity;

    @NotBlank(message = "busCompanyId.is.blank")
    private String busCompanyId;

}
