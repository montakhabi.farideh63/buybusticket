package com.snapp.buyticket.api.dto;

import lombok.Data;
import org.bson.types.ObjectId;

@Data
public class SeatOutputDto {

    private String id;
    private Integer seatNumber;
    private Boolean available;
}
