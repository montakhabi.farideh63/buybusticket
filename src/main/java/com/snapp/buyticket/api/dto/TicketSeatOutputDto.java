package com.snapp.buyticket.api.dto;

import lombok.Data;

@Data
public class TicketSeatOutputDto {

    private String id;
    private Integer seatNumber;
}
