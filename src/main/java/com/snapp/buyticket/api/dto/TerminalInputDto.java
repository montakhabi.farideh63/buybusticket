package com.snapp.buyticket.api.dto;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import org.bson.types.ObjectId;

@Data
public class TerminalInputDto {

    @NotBlank(message = "name.is.blank")
    private String name;

    @NotBlank(message = "cityId.is.blank")
    private String cityId;

    private String phone;
}
