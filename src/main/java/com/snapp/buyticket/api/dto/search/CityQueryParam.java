package com.snapp.buyticket.api.dto.search;

import lombok.Data;

@Data
public class CityQueryParam {

    private String name;
}
