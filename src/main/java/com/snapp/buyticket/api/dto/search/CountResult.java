package com.snapp.buyticket.api.dto.search;

import lombok.Data;

@Data
public class CountResult {

    private long count;
}
