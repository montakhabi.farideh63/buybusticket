package com.snapp.buyticket.api.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
public class TicketOutputDto {

    private String busModel;
    private Boolean busVip;
    private String busCompanyName;
    private Long departureDateTime;
    private String formCityName;
    private String formTerminalName;
    private String toCityName;
    private String toTerminalName;
    private List<TicketSeatOutputDto> seats;
    private List<TicketPassengerOutputDto> passengers;
    private Long pricePerSeat;

}
