package com.snapp.buyticket.api.dto;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class CityInputDto {

    @NotBlank(message = "code.is.blank")
    private String code;

    @NotBlank(message = "name.is.blank")
    private String name;
}
