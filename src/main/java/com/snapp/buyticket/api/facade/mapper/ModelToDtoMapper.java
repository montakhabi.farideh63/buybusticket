package com.snapp.buyticket.api.facade.mapper;

import com.snapp.buyticket.api.dto.*;
import com.snapp.buyticket.dal.model.*;
import org.bson.types.ObjectId;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.util.ObjectUtils;

@Mapper(componentModel = "spring")
public interface ModelToDtoMapper {

    TripOutputDto getDtoFromModel(TripModel model);

    SeatOutputDto getDtoFromModel(SeatModel model);

    BusOutputDto getDtoFromModel(BusModel model);

    CityOutputDto getDtoFromModel(CityModel model);

    TerminalOutputDto getDtoFromModel(TerminalModel model);

    TicketOutputDto getDtoFromModel(TicketModel model);

}
