package com.snapp.buyticket.api.facade.mapper;

import com.snapp.buyticket.api.dto.*;
import com.snapp.buyticket.dal.model.*;
import org.bson.types.ObjectId;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.util.ObjectUtils;

@Mapper(componentModel = "spring")
public interface DtoToModelMapper {

    @Named("objectIdToString")
    default String objectIdToString(ObjectId objectId) {
        return !ObjectUtils.isEmpty(objectId) ? objectId.toString() : null;
    }

    @Named("stringToObjectId")
    default ObjectId stringToObjectId(String str) {
        return !ObjectUtils.isEmpty(str) ? new ObjectId(str) : null;
    }

    @Mapping(target = "busCompanyId", source = "busCompanyId", qualifiedByName = "stringToObjectId")
    BusModel getModelFromDto(BusInputDto inputDto) ;

    BusCompanyModel getModelFromDto(BusCompanyInputDto inputDto) ;

    CityModel getModelFromDto(CityInputDto inputDto) ;

    @Mapping(target = "cityId", source = "cityId", qualifiedByName = "stringToObjectId")
    TerminalModel getModelFromDto(TerminalInputDto inputDto) ;

    SeatModel getModelFromDto(SeatInputDto inputDto) ;

    @Mapping(target = "busId", source = "busId", qualifiedByName = "stringToObjectId")
    @Mapping(target = "formTerminalId", source = "formTerminalId", qualifiedByName = "stringToObjectId")
    @Mapping(target = "toTerminalId", source = "toTerminalId", qualifiedByName = "stringToObjectId")
    TripModel getModelFromDto(TripInputDto inputDto) ;

    @Mapping(target = "tripId", source = "tripId", qualifiedByName = "stringToObjectId")
    TicketModel getModelFromDto(TicketInputDto inputDto);

    @Mapping(target = "id", source = "seatId")
    SeatModel getModelFromDto(TicketSeatInputDto inputDto);

    PassengerModel getModelFromDto(PassengerInputDto inputDto);
}
