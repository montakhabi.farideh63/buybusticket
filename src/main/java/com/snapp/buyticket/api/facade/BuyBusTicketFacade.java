package com.snapp.buyticket.api.facade;

import com.snapp.buyticket.api.dto.*;
import com.snapp.buyticket.api.dto.search.CityQueryParam;
import com.snapp.buyticket.api.dto.search.PageQueryParam;
import com.snapp.buyticket.api.dto.search.TerminalQueryParam;
import com.snapp.buyticket.api.dto.search.TripQueryParam;
import com.snapp.buyticket.api.facade.mapper.DtoToModelMapper;
import com.snapp.buyticket.api.facade.mapper.ModelToDtoMapper;
import com.snapp.buyticket.dal.model.*;
import com.snapp.buyticket.service.*;
import com.snapp.usermanagment.dal.model.UserModel;
import com.snapp.usermanagment.service.UserService;
import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.util.List;
import java.util.Random;


@Component
@RequiredArgsConstructor
public class BuyBusTicketFacade {

    private final BusService busService;
    private final BusCompanyService busCompanyService;
    private final CityService cityService;
    private final TerminalService terminalService;
    private final TripService tripService;
    private final SeatService seatService;
    private final UserService userService;
    private final PassengerService passengerService;
    private final TicketService ticketService;
    private final DtoToModelMapper dtoToModelMapper;
    private final ModelToDtoMapper modelToDtoMapper;

    public String addBus(BusInputDto inputDto){
        BusModel busModel = dtoToModelMapper.getModelFromDto(inputDto);
        BusCompanyModel busCompanyModel = busCompanyService.findById(inputDto.getBusCompanyId());
        busModel.setBusCompanyName(busCompanyModel.getName());
        return busService.save(busModel);
    }

    public String addBusCompany(BusCompanyInputDto inputDto){
        return busCompanyService.save(dtoToModelMapper.getModelFromDto(inputDto));
    }

    public String addCity(CityInputDto inputDto){
        return cityService.save(dtoToModelMapper.getModelFromDto(inputDto));
    }

    public String addTerminal(TerminalInputDto inputDto){
        TerminalModel terminalModel = dtoToModelMapper.getModelFromDto(inputDto);
        CityModel cityModel = cityService.findById(inputDto.getCityId());
        terminalModel.setCityName(cityModel.getName());
        return terminalService.save(terminalModel);
    }

    public String addTrip(TripInputDto inputDto){
        TripModel tripModel = dtoToModelMapper.getModelFromDto(inputDto);
        tripModel.setAvailableCapacity(inputDto.getSeats().size());
        String tripId =  tripService.save(tripModel);

        List<SeatModel> seatModelList = inputDto.getSeats().stream().map(dtoToModelMapper::getModelFromDto).toList();
        seatModelList.forEach(seatModel -> seatModel.setTripId(new ObjectId(tripId)));
        seatService.saveList(seatModelList);
        return tripId;
    }

    public String buyTicket(TicketInputDto inputDto){
        UserModel buyerUser = getBuyerUser(inputDto.getBuyerMobile());
        TicketModel tripModel = dtoToModelMapper.getModelFromDto(inputDto);
        tripModel.setPassengers(preperePassengerlist(buyerUser, tripModel.getPassengers()));
        tripModel.setUserId(new ObjectId(buyerUser.getId()));
        String ticketId = ticketService.save(tripModel);
        tripService.updateAvailableCapacity(tripModel.getTripId().toString(), tripModel.getSeats().size());
        seatService.updateAvailableStatus(tripModel.getSeats());
        return ticketId;
    }

    private List<PassengerModel> preperePassengerlist(UserModel buyerUser, List<PassengerModel> passengerModelList) {
        passengerModelList.forEach(p -> {
            if (ObjectUtils.isEmpty(p.getId())){
                p.setUserId(new ObjectId(buyerUser.getId()));
                p.setId(passengerService.save(p));
            }
            p.setGender(null);
            p.setNationalId(null);
            p.setUserId(null);
        });
        return passengerModelList;
    }

    private UserModel getBuyerUser(String buyerMobile){
        UserModel userModel = userService.findByMobile(buyerMobile);
        if(ObjectUtils.isEmpty(userModel.getId())){
            userModel.setUsername(buyerMobile);
            userModel.setMobile(buyerMobile);
            userModel.setPassword(String.valueOf(new Random().nextInt(1000000000)));
            userModel.setId(userService.addUser(userModel));
        }
        return userModel;
    }

    public Page<TripOutputDto> getAllActiveTrips(TripQueryParam tripQueryParam, PageQueryParam pageQueryParam) {
        Pageable pageable = pageQueryParam.convertPageQueryParamToPageable();
        return tripService.getAllActiveTrips(tripQueryParam, pageable).map(modelToDtoMapper::getDtoFromModel);
    }


    public List<SeatOutputDto> getSeats(String tripId) {
        return seatService.getSeatsByTripId(tripId).stream().map(modelToDtoMapper::getDtoFromModel).toList();
    }

    public List<CityOutputDto> getAllCity(CityQueryParam cityQueryParam) {
        return cityService.getAllCity(cityQueryParam).stream().map(modelToDtoMapper::getDtoFromModel).toList();
    }

    public Page<TerminalOutputDto> getAllTerminal(TerminalQueryParam terminalQueryParam, PageQueryParam pageQueryParam) {
        Pageable pageable = pageQueryParam.convertPageQueryParamToPageable();
        return terminalService.getTerminalPageByFilter(terminalQueryParam, pageable).map(modelToDtoMapper::getDtoFromModel);
    }

    public TicketOutputDto getTicket(String ticketId) {
        TicketModel ticketModel = ticketService.findById(ticketId);
        TicketOutputDto ticketOutputDto =  modelToDtoMapper.getDtoFromModel(ticketModel);

        TripModel tripModel = tripService.findById(ticketModel.getTripId().toString());
        ticketOutputDto.setDepartureDateTime(tripModel.getDepartureDateTime());
        ticketOutputDto.setFormCityName(tripModel.getFormCityName());
        ticketOutputDto.setFormTerminalName(tripModel.getFormTerminalName());
        ticketOutputDto.setToCityName(tripModel.getToCityName());
        ticketOutputDto.setToTerminalName(tripModel.getToTerminalName());
        ticketOutputDto.setPricePerSeat(tripModel.getPricePerSeat());

        BusModel busModel = busService.findById(tripModel.getBusId().toString());
        ticketOutputDto.setBusModel(busModel.getModel());
        ticketOutputDto.setBusVip(busModel.getVip());
        ticketOutputDto.setBusCompanyName(busModel.getBusCompanyName());

        return ticketOutputDto;
    }
}
