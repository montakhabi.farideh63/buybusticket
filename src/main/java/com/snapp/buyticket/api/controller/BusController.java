package com.snapp.buyticket.api.controller;

import com.snapp.buyticket.api.dto.BusInputDto;
import com.snapp.buyticket.api.facade.BuyBusTicketFacade;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/v1/bus")
@RequiredArgsConstructor
@Api(value = "API endpoints for managing Bus Entity")
public class BusController {

    private final BuyBusTicketFacade buyBusTicketFacade;

    @PostMapping("/add")
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation(value = "This method adds new bus.")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String addBus(@RequestBody @Valid BusInputDto inputDto) {
        return buyBusTicketFacade.addBus(inputDto);
    }

}
