package com.snapp.buyticket.api.controller;

import com.snapp.buyticket.api.dto.TerminalInputDto;
import com.snapp.buyticket.api.facade.BuyBusTicketFacade;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/terminal")
@RequiredArgsConstructor
@Api(value = "API endpoints for managing Terminal Entity")
public class TerminalController {

    private final BuyBusTicketFacade buyBusTicketFacade;

    @PostMapping("/add")
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation(value = "This method adds new terminal.")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String addTerminal(@RequestBody @Valid TerminalInputDto inputDto) {
        return buyBusTicketFacade.addTerminal(inputDto);
    }
}
