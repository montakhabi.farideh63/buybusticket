package com.snapp.buyticket.api.controller;

import com.snapp.buyticket.api.dto.CityInputDto;
import com.snapp.buyticket.api.dto.search.PageQueryParam;
import com.snapp.buyticket.api.dto.search.TripQueryParam;
import com.snapp.buyticket.api.facade.BuyBusTicketFacade;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/city")
@RequiredArgsConstructor
@Api(value = "API endpoints for managing City Entity")
public class CityController {

    private final BuyBusTicketFacade buyBusTicketFacade;

    @PostMapping("/add")
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation(value = "This method adds new city.")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String addCity(@RequestBody @Valid CityInputDto inputDto) {
        return buyBusTicketFacade.addCity(inputDto);
    }
}
