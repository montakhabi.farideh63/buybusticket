package com.snapp.buyticket.api.controller;

import com.snapp.buyticket.api.dto.TicketInputDto;
import com.snapp.buyticket.api.facade.BuyBusTicketFacade;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/public/v1/ticket")
@RequiredArgsConstructor
@Api(value = "API endpoints for managing Ticket Entity")
public class PublicTicketController {

    private final BuyBusTicketFacade buyBusTicketFacade;

    @PostMapping("/buy")
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation(value = "This method adds new ticket.")
    public String buyTicket(@RequestBody @Valid TicketInputDto inputDto) {
        return buyBusTicketFacade.buyTicket(inputDto);
    }
}
