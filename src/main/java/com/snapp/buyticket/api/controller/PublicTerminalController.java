package com.snapp.buyticket.api.controller;

import com.snapp.buyticket.api.dto.TerminalOutputDto;
import com.snapp.buyticket.api.dto.search.PageQueryParam;
import com.snapp.buyticket.api.dto.search.TerminalQueryParam;
import com.snapp.buyticket.api.facade.BuyBusTicketFacade;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/public/v1/terminal")
@RequiredArgsConstructor
@Api(value = "API endpoints for managing Terminal Entity")
public class PublicTerminalController {

    private final BuyBusTicketFacade buyBusTicketFacade;

    @GetMapping("/get-all")
    @ApiOperation("This method returns all terminal.")
    public Page<TerminalOutputDto> getAllTerminal(TerminalQueryParam terminalQueryParam, PageQueryParam pageQueryParam) {
        return buyBusTicketFacade.getAllTerminal(terminalQueryParam, pageQueryParam);
    }
}
