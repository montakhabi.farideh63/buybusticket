package com.snapp.buyticket.api.controller;


import com.snapp.buyticket.api.dto.SeatOutputDto;
import com.snapp.buyticket.api.dto.TicketOutputDto;
import com.snapp.buyticket.api.dto.TripOutputDto;
import com.snapp.buyticket.api.dto.search.PageQueryParam;
import com.snapp.buyticket.api.dto.search.TripQueryParam;
import com.snapp.buyticket.api.facade.BuyBusTicketFacade;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/public/v1/trip")
@RequiredArgsConstructor
@Api(value = "API endpoints for managing Trip Entity")
public class PublicTripController {

    private final BuyBusTicketFacade buyBusTicketFacade;

    @GetMapping("/get-all")
    @ApiOperation("This method returns all active trips.")
    public Page<TripOutputDto> getAll(TripQueryParam tripQueryParam, PageQueryParam pageQueryParam) {
        return buyBusTicketFacade.getAllActiveTrips(tripQueryParam, pageQueryParam);
    }

    @GetMapping("/{tripId}/get-seats")
    @ApiOperation(value = "This method return all seat of trip")
    public List<SeatOutputDto> getSeats(@PathVariable String tripId) {
        return buyBusTicketFacade.getSeats(tripId);
    }
}
