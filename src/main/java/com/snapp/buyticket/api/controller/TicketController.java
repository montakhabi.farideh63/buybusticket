package com.snapp.buyticket.api.controller;

import com.snapp.buyticket.api.dto.TicketCancelInputDto;
import com.snapp.buyticket.api.dto.TicketInputDto;
import com.snapp.buyticket.api.dto.TicketOutputDto;
import com.snapp.buyticket.api.dto.TripInputDto;
import com.snapp.buyticket.api.facade.BuyBusTicketFacade;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jakarta.validation.Valid;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/ticket")
@RequiredArgsConstructor
@Api(value = "API endpoints for managing Ticket Entity")
public class TicketController {

    private final BuyBusTicketFacade buyBusTicketFacade;

    @GetMapping("/{ticketId}/get")
    @ApiOperation(value = "This method load ticket info by id")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
    public TicketOutputDto getTicket(@PathVariable String ticketId) {
        return buyBusTicketFacade.getTicket(ticketId);
    }

    @PatchMapping("/{ticketId}/cancel")
    @ApiOperation(value = "This method cancel ticket")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
    public Boolean cancelTicket(@PathVariable String ticketId, @RequestBody @Valid TicketCancelInputDto inputDto) {
        return null;//TODO
    }
}
