package com.snapp.buyticket.api.controller;

import com.snapp.buyticket.api.dto.CityOutputDto;
import com.snapp.buyticket.api.dto.search.CityQueryParam;
import com.snapp.buyticket.api.dto.search.PageQueryParam;
import com.snapp.buyticket.api.facade.BuyBusTicketFacade;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/public/v1/city")
@RequiredArgsConstructor
@Api(value = "API endpoints for managing City Entity")
public class PublicCityController {

    private final BuyBusTicketFacade buyBusTicketFacade;

    @GetMapping("/get-all")
    @ApiOperation("This method returns all city.")
    public List<CityOutputDto> getAll(CityQueryParam cityQueryParam) {
        return buyBusTicketFacade.getAllCity(cityQueryParam);
    }
}
