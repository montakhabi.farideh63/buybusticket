package com.snapp.usermanagment.service.mapper;

import com.snapp.usermanagment.dal.entity.mongo.User;
import com.snapp.usermanagment.dal.model.UserModel;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserModelEntityMapper {

    User getEntityFromModel(UserModel model);
    UserModel getModelFromEntity(User entity);
}
