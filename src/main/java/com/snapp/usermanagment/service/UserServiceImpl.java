package com.snapp.usermanagment.service;



import com.snapp.buyticket.api.handler.exception.AlreadyRegisterException;
import com.snapp.buyticket.api.handler.exception.AuthenticationException;
import com.snapp.buyticket.api.handler.exception.NotFoundException;
import com.snapp.usermanagment.dal.entity.mongo.User;
import com.snapp.usermanagment.dal.model.UserModel;
import com.snapp.usermanagment.dal.reporitory.mongo.UserRepository;
import com.snapp.usermanagment.enums.Role;
import com.snapp.usermanagment.security.JwtTokenUtil;
import com.snapp.usermanagment.service.mapper.UserModelEntityMapper;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ObjectUtils;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserModelEntityMapper mapper;
    private final UserRepository userRepository;
    private final JwtTokenUtil jwtTokenUtil;
    private  final PasswordEncoder passwordEncoder;
    private  final  UserDetailsService userDetailsService;

    public String addUser(UserModel userModel) {
        User newUser = mapper.getEntityFromModel(userModel);
        newUser.setUsername(ObjectUtils.isEmpty(userModel.getMobile()) ? userModel.getEmail() : userModel.getMobile());
        newUser.setPassword(passwordEncoder.encode(userModel.getPassword()));
        newUser.setRole(Role.USER.getKey());

        if (userRepository.findByUsername(newUser.getUsername()).isPresent()) {
            throw new AlreadyRegisterException("User already registered: " + newUser.getUsername());
        }
        userRepository.save(newUser);
        return newUser.getId();
    }

    public String authenticateUser(UserModel userModel) throws Exception {
        final UserDetails userDetails = userDetailsService.loadUserByUsername(userModel.getUsername());
        if (userDetails != null && passwordMatches(userModel.getPassword(), userDetails.getPassword())) {
            return jwtTokenUtil.generateToken(userDetails);
        }else  {
            throw new AuthenticationException();
        }
    }

    private boolean passwordMatches(String rawPassword, String encodedPassword) {
        if(!passwordEncoder.matches(rawPassword, encodedPassword)) {
            throw new AuthenticationException("{user.password.is.not.valid}");
        }
        return true;
    }

    public UserModel findByUsername(String username){
        User loadedUser = userRepository.findByUsername(username).orElseThrow(() -> new NotFoundException("user not found : " + username));
        return mapper.getModelFromEntity(loadedUser);
    }

    public UserModel findById(String id){
        User loadedUser = userRepository.findById(id).orElseThrow(() -> new NotFoundException("user not found : " + id));
        return mapper.getModelFromEntity(loadedUser);
    }

    public Boolean updatePassword(String userId, String oldPassword, String newPassword) {
        User loadedUser = userRepository.findById(userId).orElseThrow(() -> new NotFoundException("user not found : " + userId));
        passwordMatches(oldPassword, loadedUser.getPassword());
        loadedUser.setPassword(passwordEncoder.encode(newPassword));
        userRepository.save(loadedUser);
        return true;
    }

    public String getCurrentUsername() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication.isAuthenticated()) {
            Object principal = authentication.getPrincipal();
            if (principal instanceof UserDetails) {
                return ((UserDetails) principal).getUsername();
            }
        }
        return null;
    }

    public UserModel findByMobile(String mobile){
        User loadedUser = userRepository.findByMobile(mobile).orElseGet(User::new);
        return mapper.getModelFromEntity(loadedUser);
    }

    public UserModel findByEmail(String email){
        User loadedUser = userRepository.findByEmail(email).orElseGet(User::new);
        return mapper.getModelFromEntity(loadedUser);
    }
}
