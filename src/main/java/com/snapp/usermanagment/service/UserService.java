package com.snapp.usermanagment.service;

import com.snapp.usermanagment.dal.model.UserModel;


public interface UserService {

    String addUser(UserModel userModel);
    String authenticateUser(UserModel userModel) throws Exception;
    UserModel findById(String id);
    UserModel findByUsername(String username);
    UserModel findByMobile(String mobile);
    UserModel findByEmail(String email);
    Boolean updatePassword(String userId, String oldPassword, String newPassword);
    String getCurrentUsername();
}
