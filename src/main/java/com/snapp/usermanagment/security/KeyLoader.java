package com.snapp.usermanagment.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import java.io.FileInputStream;
import java.io.InputStream;
import java.security.Key;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

@Component
public class KeyLoader {

    @Value("${jwt.privateKeyFile}")
    private String privateKeyFile;

    @Value("${jwt.keystorePassword}")
    private String keystorePassword;

    @Value("${jwt.keyAlias}")
    private String keyAlias;

    @Value("${jwt.keyPassword}")
    private String keyPassword;

    @Value("${jwt.publicKeyFile}")
    private String publicKeyFile;


    public PrivateKey loadPrivateKey() throws Exception {
        InputStream fis = loadResourceAsStream(privateKeyFile);
        KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
        keystore.load(fis, keystorePassword.toCharArray());
        fis.close();

        Key key = keystore.getKey(keyAlias, keyPassword.toCharArray());
        if (key instanceof PrivateKey) {
            return (PrivateKey) key;
        }
        throw new Exception("Private key not found in the keystore");
    }

    public PublicKey loadPublicKey() throws Exception {
        InputStream fis = loadResourceAsStream(publicKeyFile);
        CertificateFactory cf = CertificateFactory.getInstance("X.509");
        X509Certificate cert = (X509Certificate) cf.generateCertificate(fis);
        fis.close();

        return cert.getPublicKey();
    }

    private InputStream loadResourceAsStream(String resourcePath) {
        ClassLoader classLoader = ResourceLoader.class.getClassLoader();
        String fullPath = "keys/" + resourcePath;
        return classLoader.getResourceAsStream(fullPath);
    }
}
