package com.snapp.usermanagment.dal.reporitory.mongo;

import com.snapp.usermanagment.dal.entity.mongo.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface UserRepository extends MongoRepository<User,String> {
    Optional<User> findByUsername(String username);
    Optional<User> findByMobile(String mobile);
    Optional<User> findByEmail(String email);
}
