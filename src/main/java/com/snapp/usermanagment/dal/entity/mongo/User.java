package com.snapp.usermanagment.dal.entity.mongo;

import com.snapp.buyticket.dal.entity.mongo.BaseEntity;
import lombok.Data;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "User")
public class User extends BaseEntity {

    @Indexed
    private String username;

    private String email;

    private String mobile;

    private String password;

    private String role;

}
