package com.snapp.usermanagment.dal.model;

import lombok.Data;

@Data
public class UserModel {

    private String id;
    private String username;
    private String password;
    private String email;
    private String mobile;
    private String role;
}
