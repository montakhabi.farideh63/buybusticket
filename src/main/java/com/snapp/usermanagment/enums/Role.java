
package com.snapp.usermanagment.enums;

public enum Role {
    
    USER("ROLE_USER",""),
    ADMIN("ROLE_ADMIN","");

    private final String key;
    private final String text;

    Role(String key, String text) {
        this.key = key;
        this.text = text;
    }

    public String getKey() {
        return this.key;
    }

    public String getText() {
        return this.text;
    }
}