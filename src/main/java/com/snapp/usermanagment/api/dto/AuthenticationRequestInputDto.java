package com.snapp.usermanagment.api.dto;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class AuthenticationRequestInputDto {

    @NotBlank(message = "username.is.blank")
    private String username;

    @NotBlank(message = "password.is.blank")
    private String password;
}
