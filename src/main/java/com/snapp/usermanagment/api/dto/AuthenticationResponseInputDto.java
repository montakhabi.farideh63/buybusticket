package com.snapp.usermanagment.api.dto;

import lombok.Data;

@Data
public class AuthenticationResponseInputDto {

    private String token;
}
