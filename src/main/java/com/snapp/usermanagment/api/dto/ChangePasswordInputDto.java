package com.snapp.usermanagment.api.dto;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class ChangePasswordInputDto {

    @NotBlank(message = "newPassword.is.blank")
    private String newPassword;

    @NotBlank(message = "oldPassword.is.blank")
    private String oldPassword;
}
