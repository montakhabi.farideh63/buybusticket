package com.snapp.usermanagment.api.dto;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class UserRegistrationInputDto {

    @NotBlank(message = "password.is.blank")
    private String password;

    private String email;

    private String mobile;

}
