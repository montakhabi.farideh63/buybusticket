package com.snapp.usermanagment.api.controller;

import com.snapp.usermanagment.api.dto.ChangePasswordInputDto;
import com.snapp.usermanagment.api.facade.UserFacade;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/user")
@RequiredArgsConstructor
@Api(value = "API endpoints for managing User Entity")
public class UserController {

    private final UserFacade userFacade;

    @PatchMapping("/{userId}/change-password")
    @ApiOperation(value = "This method update user password.")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
    public Boolean changePassword(@PathVariable String userId, @RequestBody @Valid ChangePasswordInputDto changePasswordRequest) {
        return userFacade.changePassword(userId, changePasswordRequest);
    }
}
