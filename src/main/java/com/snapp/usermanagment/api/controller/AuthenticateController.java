package com.snapp.usermanagment.api.controller;

import com.snapp.usermanagment.api.dto.AuthenticationRequestInputDto;
import com.snapp.usermanagment.api.dto.AuthenticationResponseInputDto;
import com.snapp.usermanagment.api.dto.UserRegistrationInputDto;
import com.snapp.usermanagment.api.facade.UserFacade;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/authenticate/v1/user")
@RequiredArgsConstructor
@Api(value = "API endpoints for managing User Security")
public class AuthenticateController {

    private final UserFacade userFacade;

    @GetMapping("/get-token")
    @ApiOperation(value = "This method authenticate user.")
    public AuthenticationResponseInputDto getToken(@RequestBody @Valid AuthenticationRequestInputDto authRequest) {
        return userFacade.getToken(authRequest);
    }

    @PostMapping("/register")
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation(value = "This method add new user.")
    public String addUser(@RequestBody @Valid UserRegistrationInputDto registrationRequest) {
        return userFacade.addUser(registrationRequest);
    }
}
