package com.snapp.usermanagment.api.facade.mapper;

import com.snapp.usermanagment.api.dto.AuthenticationRequestInputDto;
import com.snapp.usermanagment.api.dto.UserRegistrationInputDto;
import com.snapp.usermanagment.dal.model.UserModel;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserModelDtoMapper {

    UserModel getModelFromDto(AuthenticationRequestInputDto inputDto);
    UserModel getModelFromDto(UserRegistrationInputDto inputDto);

}
