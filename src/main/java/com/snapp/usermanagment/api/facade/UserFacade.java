package com.snapp.usermanagment.api.facade;

import com.snapp.usermanagment.api.dto.AuthenticationRequestInputDto;
import com.snapp.usermanagment.api.dto.AuthenticationResponseInputDto;
import com.snapp.usermanagment.api.dto.ChangePasswordInputDto;
import com.snapp.usermanagment.api.dto.UserRegistrationInputDto;
import com.snapp.buyticket.api.handler.exception.InvalidDataException;
import com.snapp.usermanagment.api.facade.mapper.UserModelDtoMapper;
import com.snapp.usermanagment.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;


@Component
@RequiredArgsConstructor
public class UserFacade {

    private final UserService userService;
    private final UserModelDtoMapper mapper;

    @SneakyThrows
    public AuthenticationResponseInputDto getToken(AuthenticationRequestInputDto authRequest)  {
        String token = userService.authenticateUser(mapper.getModelFromDto(authRequest));
        AuthenticationResponseInputDto response = new AuthenticationResponseInputDto();
        response.setToken(token);
        return response;
    }

    @SneakyThrows
    public String addUser(UserRegistrationInputDto registrationRequest)  {
        if(ObjectUtils.isEmpty(registrationRequest.getEmail()) &&
                ObjectUtils.isEmpty(registrationRequest.getMobile()))
            throw new InvalidDataException("{user.email.and.mobile.is.blank}");
        return userService.addUser(mapper.getModelFromDto(registrationRequest));
    }

    @SneakyThrows
    public Boolean changePassword(String userId, ChangePasswordInputDto request )  {
        return userService.updatePassword(userId, request.getOldPassword(), request.getNewPassword());
    }
}
