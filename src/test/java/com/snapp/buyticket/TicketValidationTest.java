package com.snapp.buyticket;

import com.snapp.buyticket.api.handler.exception.InvalidDataException;
import com.snapp.buyticket.dal.model.PassengerModel;
import com.snapp.buyticket.dal.model.TicketModel;
import com.snapp.buyticket.dal.model.TripModel;
import com.snapp.buyticket.service.SeatService;
import com.snapp.buyticket.service.TripService;
import com.snapp.buyticket.service.validation.TicketValidatorImpl;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class TicketValidationTest {

    @Mock
    private TripService tripService;

    @Mock
    private SeatService seatService;

    @InjectMocks
    private TicketValidatorImpl ticketValidation;


    @Test
    void testValidation_TripInActive() {
        TicketModel ticketModel = new TicketModel();
        ticketModel.setTripId(new ObjectId("123456789012345678901234"));

        TripModel tripModel = new TripModel();
        tripModel.setId("123456789012345678901234");
        tripModel.setActive(false);

        when(tripService.findById("123456789012345678901234")).thenReturn(tripModel);

        InvalidDataException exception = assertThrows(InvalidDataException.class, () -> {
            ticketValidation.validation(ticketModel);
        });
        assertEquals("{trip.is.not.active}", exception.getReason());
    }

    @Test
    void testValidation_AvailableCapacity() {
        TicketModel ticketModel = new TicketModel();
        ticketModel.setTripId(new ObjectId("123456789012345678901234"));
        ticketModel.setPassengers(Collections.singletonList(new PassengerModel()));

        TripModel tripModel = new TripModel();
        tripModel.setId("123456789012345678901234");
        tripModel.setActive(true);
        tripModel.setAvailableCapacity(0);

        when(tripService.findById("123456789012345678901234")).thenReturn(tripModel);

        InvalidDataException exception = assertThrows(InvalidDataException.class, () -> {
            ticketValidation.validation(ticketModel);
        });
        assertEquals("{trip.available.capacity.is.less.than.number.passengers}", exception.getReason());
    }
}
