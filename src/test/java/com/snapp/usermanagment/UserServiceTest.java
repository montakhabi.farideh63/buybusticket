package com.snapp.usermanagment;

import com.snapp.buyticket.api.handler.exception.AlreadyRegisterException;
import com.snapp.usermanagment.dal.entity.mongo.User;
import com.snapp.usermanagment.dal.model.UserModel;
import com.snapp.usermanagment.dal.reporitory.mongo.UserRepository;
import com.snapp.usermanagment.enums.Role;
import com.snapp.usermanagment.service.UserServiceImpl;
import com.snapp.usermanagment.service.mapper.UserModelEntityMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {
    @Mock
    private UserModelEntityMapper mapper;

    @Mock
    private UserRepository userRepository;

    @Mock
    private  PasswordEncoder passwordEncoder;

    @InjectMocks
    private UserServiceImpl userService;

    @Test
    public void testAddUser() {
        UserModel userModel = new UserModel();
        userModel.setEmail("farideh@gmail.com");
        userModel.setPassword("farideh@M123");

        User user = new User();
        user.setId("1234567890");
        user.setUsername("farideh@gmail.com");
        user.setPassword("encoded:farideh@M123");
        user.setRole(Role.USER.getKey());

        when(mapper.getEntityFromModel(userModel)).thenReturn(user);
        when(passwordEncoder.encode("farideh@M123")).thenReturn("encoded:farideh@M123");
        when(userRepository.findByUsername("farideh@gmail.com")).thenReturn(Optional.empty());
        when(userRepository.save(any(User.class))).thenReturn(user);

        String userId = userService.addUser(userModel);

        assertEquals("1234567890", userId);
        verify(userRepository, times(1)).findByUsername("farideh@gmail.com");
        verify(userRepository, times(1)).save(user);
    }

    @Test()
    public void testAddUser_UserAlreadyExists() {
        UserModel userModel = new UserModel();
        userModel.setEmail("farideh@gmail.com");
        userModel.setPassword("farideh@M123");

        User existingUser = new User();
        existingUser.setUsername("farideh@gmail.com");

        when(mapper.getEntityFromModel(userModel)).thenReturn(existingUser);
        when(passwordEncoder.encode("farideh@M123")).thenReturn("encoded:farideh@M123");
        when(userRepository.findByUsername("farideh@gmail.com")).thenReturn(Optional.of(existingUser));

        assertThrows(AlreadyRegisterException.class, () -> {
            userService.addUser(userModel);
        });
    }
}
