package com.snapp.usermanagment;

import com.snapp.buyticket.api.handler.exception.AuthenticationException;
import com.snapp.usermanagment.dal.model.UserModel;
import com.snapp.usermanagment.security.JwtTokenUtil;
import com.snapp.usermanagment.service.UserServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AuthenticationUserServiceTest {
    @Mock
    UserDetailsService userDetailsService;

    @Mock
    JwtTokenUtil jwtTokenUtil;

    @Mock
    private PasswordEncoder passwordEncoder;

    @InjectMocks
    private UserServiceImpl userService;

    @Test
    void testAuthenticateUser_Successful() throws Exception {
        UserModel userModel = new UserModel();
        userModel.setUsername("farideh@gmail.com");
        userModel.setPassword("farideh@M123");

        UserDetails userDetails = User.withUsername("farideh@gmail.com").password("encoded:farideh@M123").roles("USER").build();

        when(userDetailsService.loadUserByUsername(anyString())).thenReturn(userDetails);
        when(passwordEncoder.matches("farideh@M123","encoded:farideh@M123")).thenReturn(true);
        when(jwtTokenUtil.generateToken(any())).thenReturn("jwt_token");

        String token = userService.authenticateUser(userModel);

        assertEquals("jwt_token", token);
        verify(userDetailsService, times(1)).loadUserByUsername("farideh@gmail.com");
        verify(jwtTokenUtil, times(1)).generateToken(userDetails);
    }

    @Test
    void testAuthenticateUser_Failure() throws Exception {
        UserModel userModel = new UserModel();
        userModel.setUsername("farideh@gmail.com");
        userModel.setPassword("farideh@M123");

        when(userDetailsService.loadUserByUsername(anyString())).thenReturn(null);
        // when(userDetailsService.loadUserByUsername(null)).thenThrow(AuthenticationException.class);

        assertThrows(AuthenticationException.class, () -> {
            userService.authenticateUser(userModel);
        });

        verify(userDetailsService, times(1)).loadUserByUsername("farideh@gmail.com");
        verify(jwtTokenUtil, never()).generateToken(any());
    }
}
