# Bus Ticket Booking System

## Overview
This project aims to provide a comprehensive solution for booking bus tickets online. It is designed as a microservices architecture, consisting of three main microservices:

1. **User Management Microservice**: Handles user authentication and management.
2. **Buy Ticket Microservice**: Facilitates the process of purchasing bus tickets.
3. **Payment Microservice**: Manages payment transactions for booked tickets.

For the purpose of this assignment, an MVP (Minimum Viable Product) version of the User Management Microservice and Buy Ticket Microservice has been implemented.

### Note:
To streamline development and testing, the User Management Microservice is integrated within the same project instead of being implemented as a separate project.

## Security
Security measures have been implemented to ensure the confidentiality and integrity of user data and transactions:

- **HTTPS**: Communication between clients and the server is secured using HTTPS protocol.
- **Spring Security with JWT Tokens**: Authentication and authorization are enforced using Spring Security with JSON Web Tokens (JWT). Access to resources is role-based, with two roles defined: ADMIN and USER.

## REST API and Swagger Documentation
The REST API endpoints are documented using Swagger. You can explore and interact with the API documentation using the Swagger UI:

[Swagger Documentation](https://localhost:8081/swagger-ui/index.html)

## End-to-End Testing
For end-to-end testing purposes, a Postman collection file named `buyBusTicket.postman_collection` is provided. You can import this collection into Postman to execute and automate test scenarios.

[buyBusTicket.postman_collection](https://drive.google.com/file/d/1FuMSCwAImxaGNnzkrzBNB3OCHpP11Hlj/view?usp=drive_link)

## Database
This project utilizes MongoDB as the database solution. If you need to inspect or interact with the database, you can use the provided export database file.

[Export Database File](https://drive.google.com/file/d/1VxY_JflDNOZ9PJCDnheMxmyVUbGhH1Vy/view?usp=drive_link)
